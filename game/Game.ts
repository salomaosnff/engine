/// <reference path= "Engine/Engine.ts" />
function toHEX(number)
{
    if (number < 0)
    {
        number = 0xFFFFFFFF + number + 1;
    }

    return number.toString(16).toUpperCase();
}
class Game extends Engine{  // Game é filho de Engine
    private static canvas;
    private static ctx;
    private static x   =   0;
    private static y   =   0;
    private static hue =   0;

    public static FPS = 60;

    public static Start(e): void {  // Ao Carregar
        this.canvas = document.getElementById("game");
        this.ctx    = this.canvas.getContext("2d");
    }

    public static Update(data:Object): void { // A cada Frame

        var ctx:CanvasRenderingContext2D = this.ctx,
            dt = data.deltaTime;

        // ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);



        if(
            Keyboard.keyIsDown("UP") || Keyboard.keyIsDown("DOWN") ||
            Keyboard.keyIsDown("LEFT") || Keyboard.keyIsDown("RIGHT")
        ){
            this.hue = this.hue < 360 ? this.hue + dt * .1 : 0;
        }

        ctx.fillStyle = changeHue("#333333", this.hue);

        if(Keyboard.keyIsDown("UP")) this.y -= dt * .1;
        if(Keyboard.keyIsDown("DOWN")) this.y += dt * .1;

        if(Keyboard.keyIsDown("LEFT")) this.x -= dt * .1;
        if(Keyboard.keyIsDown("RIGHT")) this.x += dt * .1;

        ctx.fillRect(this.x, this.y, 16, 16);

    }
}


Engine.setApp(Game);