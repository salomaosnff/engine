var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path= "Engine/Engine.ts" />
function toHEX(number) {
    if (number < 0) {
        number = 0xFFFFFFFF + number + 1;
    }
    return number.toString(16).toUpperCase();
}
var Game = (function (_super) {
    __extends(Game, _super);
    function Game() {
        _super.apply(this, arguments);
    }
    Game.Start = function (e) {
        this.canvas = document.getElementById("game");
        this.ctx = this.canvas.getContext("2d");
    };
    Game.Update = function (data) {
        var ctx = this.ctx, dt = data.deltaTime;
        // ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        if (Keyboard.keyIsDown("UP") || Keyboard.keyIsDown("DOWN") ||
            Keyboard.keyIsDown("LEFT") || Keyboard.keyIsDown("RIGHT")) {
            this.hue = this.hue < 360 ? this.hue + dt * .1 : 0;
        }
        ctx.fillStyle = changeHue("#333333", this.hue);
        if (Keyboard.keyIsDown("UP"))
            this.y -= dt * .1;
        if (Keyboard.keyIsDown("DOWN"))
            this.y += dt * .1;
        if (Keyboard.keyIsDown("LEFT"))
            this.x -= dt * .1;
        if (Keyboard.keyIsDown("RIGHT"))
            this.x += dt * .1;
        ctx.fillRect(this.x, this.y, 16, 16);
    };
    Game.x = 0;
    Game.y = 0;
    Game.hue = 0;
    Game.FPS = 60;
    return Game;
}(Engine));
Engine.setApp(Game);
//# sourceMappingURL=Game.js.map