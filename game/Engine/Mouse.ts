/// <reference path="Engine.ts" />

interface Object{
    some(callback:Function):any;
    size():number;
}

Object.prototype.some = function (callback:Function){
    var keys  = Object.keys(this);
    var i     = 0;
    var founds = {};

    while(i < keys.length){
        var key = keys[i];
        var item = this[key];

        var ret = callback(item, key);

        if(ret === true) founds[key] = item;

        i++;
    }

    return founds;
};

Object.prototype.size = function(){
    return Object.keys(this).length;
};

enum MOUSE {
    PRIMARY = 0,
    WHELL = 1,
    SECONDARY = 2
}
class Mouse{

    private static map = {};
    private static event;
    private static thead;
    private static fps = 1000 / 30;
    private static coords = {
        x : 0,
        y : 0
    };

    public static init(){
        var that = this;

        window.onmousedown = window.onmouseup = function (e) {
            that.event = e || event;

            that.map[that.event.button] = that.event.type == "mousedown";
        };

        window.onmousemove = function(e){
            that.map["move"] = true;
            that.coords = {
                x:e.clientX,
                y:e.clientY
            };
            window.clearInterval(that.thead);

            that.thead = window.setTimeout(function () {
                that.map["move"] = false;
            }, 1000 / Engine.FPS);
        };
    }

    public static isDown(buttonCode:number = null){

        if(typeof buttonCode == "number"){
            return this.map[buttonCode] === true;
        } else {
            var buttons = this.map.some(function (button) {
                return button === true;
            });

            return buttons.size() > 0;
        }

    }

    public static isUp(buttonCode:number = null){
        return !this.isDown(buttonCode);
    }

    public static isMove(){
        return this.map["move"] === true;
    }

    public static getPos(axis:string = null){
        if(this.coords.hasOwnProperty(axis)){
            return this.coords[axis];
        }else{
            return this.coords;
        }
    }
}