/// <reference path= "Engine.ts" />

function require(...files){
    var args = Array.prototype.slice.call(arguments);

    function append(fileName){
        var script = document.createElement("script");

        script.src = fileName+".js";
        document.head.appendChild(script);
    }

    if(args.length > 0){
        for(var i in args){
            if(args.hasOwnProperty(i)) append(args[i]);
        }
    }
}

// Includes
require(
    "Engine/Functions",
    "Engine/FramesPerSecond",
    "Engine/Keyboard",
    "Engine/Mouse",
    "Engine/Engine",
    "Game"
);

// Start the Engine
window.onload = (e) =>{
    Engine.Load(e);
};