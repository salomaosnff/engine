var FramesPerSecond = (function () {
    function FramesPerSecond(callback, fps, duration) {
        var _this = this;
        if (fps === void 0) { fps = 24; }
        if (duration === void 0) { duration = "EVER"; }
        this.callback = callback;
        this.fps = fps;
        this.duration = duration;
        this.lastUpdate = 0;
        this.frame = -1;
        this.isPlaying = false;
        var that = this;
        this.delay = 1000 / fps;
        this.loop = function (timestamp) {
            that.timeStart = that.timeStart || timestamp;
            var seg = Math.floor((timestamp - that.timeStart) / that.delay);
            var timeleft = Math.max(0, duration - (timestamp - that.timeStart));
            if (seg > _this.frame) {
                that.frame = seg;
                that.callback({
                    seg: seg,
                    timeStart: that.timeStart,
                    time: timestamp,
                    deltaTime: (timestamp - that.lastUpdate),
                    frame: that.frame,
                    frameRate: that.fps,
                    duration: that.duration,
                    timeLeft: typeof that.duration != "number" ? Infinity : timeleft
                });
                that.lastUpdate = timestamp;
            }
            if (typeof that.duration != "number" || timeleft > 0) {
                window.requestAnimationFrame(that.loop);
            }
        };
    }
    FramesPerSecond.prototype.setFPS = function (newFPS) {
        if (newFPS === null)
            return this.fps;
        this.fps = newFPS;
        this.delay = 1000 / this.fps;
        this.frame = -1;
        this.timeStart = null;
    };
    FramesPerSecond.prototype.play = function () {
        if (!this.isPlaying) {
            this.isPlaying = true;
            this.tref = requestAnimationFrame(this.loop);
        }
    };
    FramesPerSecond.prototype.stop = function () {
        if (this.isPlaying) {
            cancelAnimationFrame(this.tref);
            this.isPlaying = false;
            this.timeStart = null;
            this.frame = -1;
        }
    };
    return FramesPerSecond;
}());
//# sourceMappingURL=FramesPerSecond.js.map