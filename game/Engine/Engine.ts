/// <reference path= "Functions.ts" />
/// <reference path="FramesPerSecond.ts" />
/// <reference path="Keyboard.ts" />
/// <reference path="Mouse.ts" />

abstract class Engine{
    static get deltaTime(): number {
        return this._deltaTime;
    }
    private static _deltaTime = 0;
    protected static FPS = 24;
    private static App;
    private fpsFrames:FramesPerSecond;

    public Start(e):void{}
    public Update(data:Object):void{}

    public static setApp(App){
        Engine.App = App;
        Engine.FPS = Engine.App.FPS;
    }

    public static Load(e):void{

        Engine.Keyboard.init();
        Engine.Mouse.init();
        Engine.App.Start(e);
        Engine.App.fpsFrames = new FramesPerSecond((data)=>{
            Engine.App.Update(data);

            Engine._deltaTime = data._deltaTime;
        }, Engine.App.FPS);
        Engine.App.fpsFrames.play();
    }

    public stop():void{
        this.fpsFrames.stop();
    }

    public static DeltaTime = DeltaTime;
    public static Keyboard = Keyboard;
    public static Mouse = Mouse;
}
