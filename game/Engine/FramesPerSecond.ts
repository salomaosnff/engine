class FramesPerSecond{

    private delay;
    private timeStart;
    private lastUpdate : 0;
    private frame = -1;
    private tref;
    private loop;
    private isPlaying = false;

    constructor(
        private callback:any,
        private fps = 24,
        private duration:any = "EVER"
    ){
        var that = this;

        this.delay = 1000 / fps;

        this.loop = (timestamp) => {

            that.timeStart = that.timeStart || timestamp;

            var seg = Math.floor((timestamp - that.timeStart) / that.delay);
            var timeleft = Math.max(0, duration - (timestamp - that.timeStart));

            if (seg > this.frame) {

                that.frame = seg;

                that.callback({
                    seg:seg,
                    timeStart: that.timeStart,
                    time:timestamp,
                    deltaTime: (timestamp - that.lastUpdate),
                    frame:that.frame,
                    frameRate: that.fps,
                    duration:that.duration,
                    timeLeft: typeof that.duration != "number" ? Infinity : timeleft
                });

                that.lastUpdate = timestamp;
            }

            if(typeof that.duration != "number" || timeleft > 0){
                window.requestAnimationFrame(that.loop);
            }

        };
    }


    public setFPS(newFPS){

        if (newFPS === null) return this.fps;

        this.fps = newFPS;

        this.delay = 1000 / this.fps;
        this.frame = -1;
        this.timeStart = null;
    }

    public play(){
        if (!this.isPlaying) {
            this.isPlaying = true;
            this.tref = requestAnimationFrame(this.loop);
        }
    }

    public stop(){
        if (this.isPlaying) {
            cancelAnimationFrame(this.tref);

            this.isPlaying = false;
            this.timeStart = null;
            this.frame = -1;
        }
    }
}