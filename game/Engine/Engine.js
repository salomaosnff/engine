/// <reference path= "Functions.ts" />
/// <reference path="FramesPerSecond.ts" />
/// <reference path="Keyboard.ts" />
/// <reference path="Mouse.ts" />
var Engine = (function () {
    function Engine() {
    }
    Object.defineProperty(Engine, "deltaTime", {
        get: function () {
            return this._deltaTime;
        },
        enumerable: true,
        configurable: true
    });
    Engine.prototype.Start = function (e) { };
    Engine.prototype.Update = function (data) { };
    Engine.setApp = function (App) {
        Engine.App = App;
        Engine.FPS = Engine.App.FPS;
    };
    Engine.Load = function (e) {
        Engine.Keyboard.init();
        Engine.Mouse.init();
        Engine.App.Start(e);
        Engine.App.fpsFrames = new FramesPerSecond(function (data) {
            Engine.App.Update(data);
            Engine._deltaTime = data._deltaTime;
        }, Engine.App.FPS);
        Engine.App.fpsFrames.play();
    };
    Engine.prototype.stop = function () {
        this.fpsFrames.stop();
    };
    Engine._deltaTime = 0;
    Engine.FPS = 24;
    Engine.DeltaTime = DeltaTime;
    Engine.Keyboard = Keyboard;
    Engine.Mouse = Mouse;
    return Engine;
}());
//# sourceMappingURL=Engine.js.map