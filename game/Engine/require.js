/// <reference path= "Engine.ts" />
function require() {
    var files = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        files[_i - 0] = arguments[_i];
    }
    var args = Array.prototype.slice.call(arguments);
    function append(fileName) {
        var script = document.createElement("script");
        script.src = fileName + ".js";
        document.head.appendChild(script);
    }
    if (args.length > 0) {
        for (var i in args) {
            if (args.hasOwnProperty(i))
                append(args[i]);
        }
    }
}
// Includes
require("Engine/Functions", "Engine/FramesPerSecond", "Engine/Keyboard", "Engine/Mouse", "Engine/Engine", "Game");
// Start the Engine
window.onload = function (e) {
    Engine.Load(e);
};
//# sourceMappingURL=require.js.map