/// <reference path="Engine.ts" />
Object.prototype.some = function (callback) {
    var keys = Object.keys(this);
    var i = 0;
    var founds = {};
    while (i < keys.length) {
        var key = keys[i];
        var item = this[key];
        var ret = callback(item, key);
        if (ret === true)
            founds[key] = item;
        i++;
    }
    return founds;
};
Object.prototype.size = function () {
    return Object.keys(this).length;
};
var MOUSE;
(function (MOUSE) {
    MOUSE[MOUSE["PRIMARY"] = 0] = "PRIMARY";
    MOUSE[MOUSE["WHELL"] = 1] = "WHELL";
    MOUSE[MOUSE["SECONDARY"] = 2] = "SECONDARY";
})(MOUSE || (MOUSE = {}));
var Mouse = (function () {
    function Mouse() {
    }
    Mouse.init = function () {
        var that = this;
        window.onmousedown = window.onmouseup = function (e) {
            that.event = e || event;
            that.map[that.event.button] = that.event.type == "mousedown";
        };
        window.onmousemove = function (e) {
            that.map["move"] = true;
            that.coords = {
                x: e.clientX,
                y: e.clientY
            };
            window.clearInterval(that.thead);
            that.thead = window.setTimeout(function () {
                that.map["move"] = false;
            }, 1000 / Engine.FPS);
        };
    };
    Mouse.isDown = function (buttonCode) {
        if (buttonCode === void 0) { buttonCode = null; }
        if (typeof buttonCode == "number") {
            return this.map[buttonCode] === true;
        }
        else {
            var buttons = this.map.some(function (button) {
                return button === true;
            });
            return buttons.size() > 0;
        }
    };
    Mouse.isUp = function (buttonCode) {
        if (buttonCode === void 0) { buttonCode = null; }
        return !this.isDown(buttonCode);
    };
    Mouse.isMove = function () {
        return this.map["move"] === true;
    };
    Mouse.getPos = function (axis) {
        if (axis === void 0) { axis = null; }
        if (this.coords.hasOwnProperty(axis)) {
            return this.coords[axis];
        }
        else {
            return this.coords;
        }
    };
    Mouse.map = {};
    Mouse.fps = 1000 / 30;
    Mouse.coords = {
        x: 0,
        y: 0
    };
    return Mouse;
}());
//# sourceMappingURL=Mouse.js.map