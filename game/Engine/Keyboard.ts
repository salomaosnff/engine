let indexOf = function(obj, value:any):any{

    for(var i in obj){
        if(obj.hasOwnProperty(i)){
            if(obj[i] === value) return i;
        }
    }

    return -1;
};

class Keyboard{
    private static map = {};
    private static event;
    private static keyMap:Object = {
        8 : "BACKSPACE",
        13 : "ENTER",
        32 : "SPACE",

        27 : "ESC",

        112 : "F1",
        113 : "F2",
        114 : "F3",
        115 : "F4",
        116 : "F5",
        117 : "F6",
        118 : "F7",
        119 : "F8",
        120 : "F9",
        121 : "F10",
        122 : "F11",
        123 : "F12",

        145 : "SCROLL_LOCK",
        19 : "PAUSE/BREAK",
        45 : "INSERT",
        36 : "HOME",
        33 : "PAGE_UP",
        46 : "DELETE",
        35 : "END",
        34 : "PAGE_DOWN",

        38 : "UP",
        40 : "DOWN",
        37 : "LEFT",
        39 : "RIGHT",

        222 : "'",
        48 : "0",
        49 : "1",
        50 : "2",
        51 : "3",
        52 : "4",
        53 : "5",
        54 : "6",
        55 : "7",
        56 : "8",
        57 : "9",
        173 : "-",
        61 : "=",

        9 : "TAB",
        20 : "CAPS_LOCK",
        16 : "SHIFT",
        17 : "CTRL",
        18 : "ALT",

        219 : "[",
        221 : "]",

        144 : "NUM_LOCK",
        111 : "DIVIDE",
        106 : "MULTIPLY",
        109 : "SUBTRACT",
        107 : "ADD",
        110 : "DECIMAL_POINT",

        96 : "NUM_0",
        97 : "NUM_1",
        98 : "NUM_2",
        99 : "NUM_3",
        100 : "NUM_4",
        101 : "NUM_5",
        102 : "NUM_6",
        103 : "NUM_7",
        104 : "NUM_8",
        105 : "NUM_9",

        65 : "A",
        66 : "B",
        67 : "C",
        68 : "D",
        69 : "E",
        70 : "F",
        71 : "G",
        72 : "H",
        73 : "I",
        74 : "J",
        75 : "K",
        76 : "L",
        77 : "M",
        78 : "N",
        79 : "O",
        80 : "P",
        81 : "Q",
        82 : "R",
        83 : "S",
        84 : "T",
        85 : "U",
        86 : "V",
        87 : "W",
        88 : "X",
        89 : "Y",
        90 : "Z",

        220 : "\\",
        188 : ",",
        190 : ".",
        59 : ";",
        191 : "/"
    };

    public static updateKeyboard(e){
        Keyboard.event = e || event;
        Keyboard.map[Keyboard.event.keyCode] = Keyboard.event.type == "keydown";
    }

    public static isDown(keyCode:number){
        return Keyboard.map[keyCode] == true;
    }

    public static isUp(keyCode:number){
        return (Keyboard.map[keyCode] == false || Keyboard.map[keyCode] == null);
    }

    public static keyIsDown(key){
        if(typeof key === "string"){
            let keyCode = indexOf(Keyboard.keyMap, key);

            return Keyboard.isDown(keyCode)
        }
    }

    public static keyIsUp(key){
        if(typeof key === "string"){
            let keyCode = indexOf(Keyboard.keyMap, key);

            return Keyboard.isUp(keyCode)
        }
    }

    public static init(){
        window.onkeydown = window.onkeyup = Keyboard.updateKeyboard;
    }
}